package com.zultys.markapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

public class JavaActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout buttonAddParticipants;
    private LinearLayout buttonParticipants;
    private LinearLayout buttonEndConference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ConstraintLayout conferencePanel = findViewById(R.id.conferencePanel);

        buttonAddParticipants = conferencePanel.findViewById(R.id.llAddParticipants);
        buttonParticipants = conferencePanel.findViewById(R.id.llParticipants);
        buttonEndConference = conferencePanel.findViewById(R.id.llEndConference);

        Button left = findViewById(R.id.buttonLeft);
        Button middle = findViewById(R.id.buttonMiddle);
        Button right = findViewById(R.id.buttonEnd);

        setClickListeners(buttonAddParticipants, buttonParticipants, buttonEndConference, left, middle, right);
    }

    private void setClickListeners(View... views) {
        for (int i = 0; i < views.length; i++) {
            views[i].setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        String toast = null;
        switch (v.getId()) {
            case R.id.llAddParticipants:
                toast = "Add participants";
                break;
            case R.id.llParticipants:
                toast = "Participants";
                break;
            case R.id.llEndConference:
                toast = "End Conference";
                break;
            case R.id.buttonLeft:
                if (buttonAddParticipants.getVisibility() == View.VISIBLE)
                    buttonAddParticipants.setVisibility(View.GONE);
                else buttonAddParticipants.setVisibility(View.VISIBLE);
                break;
            case R.id.buttonMiddle:
                if (buttonParticipants.getVisibility() == View.VISIBLE)
                    buttonParticipants.setVisibility(View.GONE);
                else buttonParticipants.setVisibility(View.VISIBLE);
                break;
            case R.id.buttonEnd:
                if (buttonEndConference.getVisibility() == View.VISIBLE)
                    buttonEndConference.setVisibility(View.GONE);
                else buttonEndConference.setVisibility(View.VISIBLE);
                break;
            default:
                System.out.println("default");
        }
        if (toast != null)
            Toast.makeText(this, toast, Toast.LENGTH_SHORT).show();
    }
}
